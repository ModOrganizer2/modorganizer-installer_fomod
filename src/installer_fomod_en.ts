<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>FomodInstallerDialog</name>
    <message>
        <location filename="fomodinstallerdialog.ui" line="14"/>
        <source>FOMOD Installer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="25"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="55"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="62"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="76"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="83"/>
        <source>&lt;a href=&quot;#&quot;&gt;Link&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="183"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="196"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="206"/>
        <location filename="fomodinstallerdialog.cpp" line="1478"/>
        <location filename="fomodinstallerdialog.cpp" line="1618"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.ui" line="213"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="215"/>
        <location filename="fomodinstallerdialog.cpp" line="259"/>
        <source>Failed to parse ModuleConfig.xml. See console for details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="226"/>
        <source>ModuleConfig.xml missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="466"/>
        <source>At least one condition was successful in an &apos;OR&apos; clause!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="474"/>
        <source>All conditions were successful in an &apos;AND&apos; clause!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="476"/>
        <source>No conditions were successful in an &apos;OR&apos; clause!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="484"/>
        <source>invalid plugin state %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="490"/>
        <source>Missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="491"/>
        <source>Inactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="492"/>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="497"/>
        <source>Success: The file &apos;%1&apos; was marked %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="500"/>
        <source>Missing requirement: The file &apos;%1&apos; should be %2, but was %3!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="566"/>
        <source>Success: The required version of %1 is %2, and was detected as %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="569"/>
        <source>Missing requirement: The required version of %1 is %2, but was detected as %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="689"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Link&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="711"/>
        <source>unsupported order type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="729"/>
        <source>unsupported group type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="989"/>
        <source>All components in this group are required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1030"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1059"/>
        <source>Select one or more of these options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1357"/>
        <source>The flag &apos;%1&apos; matched &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1359"/>
        <source>The flag &apos;%1&apos; did not match &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1368"/>
        <source>The condition was not matched and is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1369"/>
        <source>The value exists but was not matched.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1450"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1452"/>
        <source>This button is disabled because the following group(s) need a selection: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1478"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1538"/>
        <source>This component is required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1544"/>
        <source>It is recommended you enable this component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1550"/>
        <source>Optional component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1555"/>
        <source>This component is not usable in combination with other installed plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fomodinstallerdialog.cpp" line="1563"/>
        <source>You may be experiencing instability in combination with other installed plugins</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstallerFomod</name>
    <message>
        <location filename="installerfomod.cpp" line="44"/>
        <source>Installer for xml based fomod archives.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerfomod.cpp" line="245"/>
        <source>Installation as fomod failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerfomod.cpp" line="269"/>
        <source>image formats not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerfomod.cpp" line="271"/>
        <location filename="installerfomod.cpp" line="282"/>
        <source>invalid problem key %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerfomod.cpp" line="279"/>
        <source>This indicates that files from dlls/imageformats are missing from your MO installation or outdated. Images in installers may not be displayed. Please re-install MO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
